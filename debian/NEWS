libpoe-perl (2:1.2690-1) unstable; urgency=low

  In version 1.020 of POE (libpoe-perl version 2:1.020), there was
  a split upstream of several Loop modules into their own packages.
  What this means is that the following modules no longer come
  bundled with POE but must be installed manually.

  * POE::Loop::Tk (available as libpoe-loop-tk-perl)
  * POE::Loop::Event (available as libpoe-loop-event-perl)
  * POE::Loop::Gtk (a candidate for removal from Debian; it needs
    libgtk-perl, which only exists in oldstable)

  It is believed that the removal of these loops will not directly
  impact any modules in Debian. However, if anything breaks, please
  contact the pkg-perl team or ask the maintainer to add the loop
  implementation to their dependencies explicitly.

  Also note that this release includes a change that breaks backward
  compatibility on a relatively unused feature. You are affected if you
  use ARG0 or ARG1 in a POE::Component::Server::TCP ClientConnected
  callback (for details, see RT#47855)

 -- Jonathan Yu <frequency@cpan.org>  Fri, 28 Aug 2009 08:14:07 -0400
